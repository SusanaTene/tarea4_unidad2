#Crear un programa en Python que utilice la librería pygame y todo el código de la "Tarea 3"
# para dibujar un plano cartesiano y representar gráficamente los objetos Punto y los objetos
# Rectángulo creados en la tarea anterior. La salida del programa debe ser capturada en una imagen
# (similar a la mostrada al final) y guardada en el mismo repositorio gitlab de esta tarea.

print("UNIVERSIDAD NACIONAL DE LOJA")
print("Pedagogía de la Informática")
print("    Autora: Susana Tene")
print("    Fecha: 6 de Julio del 2020")

print("Ejercicio: Cree un módulo python llamado GEOMETRÍA que marque los siguientes puntos:")

import math
import pygame
pygame.init()

# Definir algunos colores
BLANCO =(255,255, 255)
NEGRO = (0,0,0)
ROJO = (255,0,0)
VERDE =(0,255,0)
AZUL =(0,0,255)


dimensiones = (700, 500)
pantalla = pygame.display.set_mode(dimensiones)

pygame.display.set_caption("GEOMETRÍA")


pantalla.fill(BLANCO)


class Punto:
    x = 0
    y = 0

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def draw(self, surface):
        pygame.draw.rect(surface, rect, width=0)
        return rect

    for desplazar_y in range(0, 100, 10):
        pygame.draw.rect(pantalla, VERDE, [150, 100, 150, 100], 4)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return "I"
        elif self.x < 0 and self.y > 0:
            return "II"
        elif self.x < 0 and self.y < 0:
            return "III"
        elif self.x > 0 and self.y < 0:
            return "IV"
        elif self.x != 0 and self.y == 0:
            return "eje X"
        elif self.x == 0 and self.y != 0:
            return "eje Y"
        else:
            return "origen"
        return self.x and self.y


    def vector(self, p):
        v = Punto(p.x - self.x, p.y - self.y)
        return v

    def distancia(self, p):
        d = math.sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        return d

    def __str__(self):
        return "({}, {})".format(self.x, self.y)


class Rectángulo:
    punto_inicial = None
    punto_final = None

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def área(self):
        return self.base() * self.altura()

if __name__ == "__main__":

    A = Punto(2,3)
    B = Punto(5,5)
    C = Punto(-3, -1)
    D = Punto(0,0)

    print(f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}")
    print(f"El punto {B} se encuentra en el cuadrante {C.cuadrante()}")
    print(f"El punto {D} se encuentra {D.cuadrante()}")

    print(f" La distancia entre el punto {A} y {B} es {A.distancia(B)}")
    print(f" La distancia entre el punto {B} y {A} es {B.distancia(A)}")

    da = A.distancia(D)
    db = B.distancia(D)
    dc = C.distancia(D)
    if da > db and da > dc:
        print(f" El punto{A} se encuentra mas lejos del origen")
    elif db > da and db > dc:
        print(f" El punto{B} se encuentra mas lejos del origen")
    else:
        print(f" El punto{C} se encuentra mas lejos del origen")

    rect = Rectángulo(A, B)
    print("La base del rectángulo es {}".format(rect.base()))
    print("La altura del rectángulo es {}".format(rect.altura()))
    print("El área del rectángulo es {}".format(rect.área ()))

    pygame.display.flip()

pygame.quit()